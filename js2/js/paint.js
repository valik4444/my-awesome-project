
var cnv = document.getElementById('canvas'),
	ctx = cnv.getContext('2d'),
	isMouseDown = false,
	coords = [],
	color = 'black',
	size = 2,
	marginTop = 30,
	marginLeft = 100;

	cnv.width = cnv.clientWidth;
	cnv.height = cnv.clientHeight;

	ctx.fillStyle = 'white';
	ctx.fillRect(0, 0, cnv.width, cnv.height);

	ctx.beginPath();
	ctx.fillStyle = color;
	ctx.strokeStyle = color;

cnv.addEventListener('mousedown', function(){
	isMouseDown = true;
});

cnv.addEventListener('mouseup', function(){
	isMouseDown = false;
	ctx.beginPath();
	coords.push('mouseup');
});


ctx.lineWidth = size * 2;
cnv.addEventListener('mousemove', function(e){
	if((e.clientX > cnv.width + marginLeft - 3) || (e.clientY > cnv.height + marginTop - 3) || (e.clientX < 33) || (e.clientY <33))
		{ctx.beginPath();
		isMouseDown = false;}
	if (isMouseDown) {
		
		coords.push([e.clientX - marginLeft, e.clientY - marginTop, color, size]);

		ctx.lineTo(e.clientX - marginLeft, e.clientY - marginTop);
		ctx.stroke();

		ctx.beginPath();
		ctx.arc(e.clientX - marginLeft, e.clientY - marginTop, size, 0, Math.PI * 2);
		ctx.fill();

		ctx.beginPath();
		ctx.moveTo(e.clientX - marginLeft, e.clientY - marginTop);
	}
	
});

cnv.addEventListener('mouseover', function(){
	var val = document.getElementById('bg');
	val.setAttribute('style', 'opacity: 0.2;');
});

cnv.addEventListener('mouseout', function(){
	var val = document.getElementById('bg');
	val.setAttribute('style', 'opacity: 1;');
});



function clearSheet() {
	ctx.fillStyle = 'white';
	ctx.fillRect(0, 0, cnv.width, cnv.height);

	ctx.beginPath();
	ctx.fillStyle = color;
}

function clearAll() {
	coords = [];
	clearSheet();
};

function save() {
	localStorage.setItem('coords', JSON.stringify(coords));
}

function replay() {
	coords = JSON.parse(localStorage.getItem('coords'));
	clearSheet();

	var timer = setInterval(function() {
		if (!coords.length)
		{
			clearInterval(timer);
			ctx.beginPath();
			return;
		}

		var crd = coords.shift(),
		e = {
			clientX: crd["0"],
			clientY: crd["1"],
			color: crd["2"],
			size: crd["3"]
		};
		ctx.lineWidth = e.size * 2;

		ctx.fillStyle = e.color;
		ctx.strokeStyle = e.color;

		ctx.lineTo(e.clientX, e.clientY);
		ctx.stroke();

		ctx.beginPath();
		ctx.arc(e.clientX, e.clientY, e.size, 0, Math.PI * 2);
		ctx.fill();

		ctx.beginPath();
		ctx.moveTo(e.clientX, e.clientY);

	}, 20)
}


function setColor(a) {
	color = a;
	ctx.fillStyle = a;
	ctx.strokeStyle = a;
	changeColor(a);
}

function sz(a) {
	size = a/2;
	ctx.lineWidth = a;
	document.getElementById("sizepic").value = a;
	document.getElementById('activesize').value = a;
}

function colorPick() {
	setColor(document.form.colorpic.value);
	changeColor(document.form.colorpic.value);
}

function changeColor(color){
var col = document.getElementById('activecolor');
col.setAttribute('style', 'background-color: '+ color + ';');
col.setAttribute('title', color);
};

function sizePic() {
	sz(document.getElementById("sizepic").value);
	var size;
	size = document.getElementById('activesize');
	size.value = document.getElementById("sizepic").value;
}

function changeSize() {
	var size = document.getElementById('activesize');
	document.getElementById("sizepic").value = size.value;
	sz(size.value);

}