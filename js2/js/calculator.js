var firstOperand = 0,
	operation = '',
	isEqual = false,
	isDot = false,
	result = 0,
	operationActive = false;

function insert(num) {
	var value = document.form.textwiew.value;
		
	

	if ((num == '.') && (isDot == false))
		{
			if (!value)
				return;
			isDot = true;
			document.form.textwiew.value = document.form.textwiew.value + num;
			return;
		}
	if ((num == '.') && (isDot == true))
		return;

	if (num == '+' || num == '-' || num == '*' || num == '/') {
		if (operationActive)
			return;
		if (!value)
			return;
		operationActive = true;
		isEqual = false;
		isDot = true;
		if (firstOperand && operation) {
			equal();
			operation = num;
			document.form.textwiew.value = document.form.textwiew.value + ' ' + num;
			return;
		}
		firstOperand = document.form.textwiew.value;
		document.form.textwiew.value = document.form.textwiew.value + ' ' + num;
		operation = num;
		return;
	}
	if (operationActive || isEqual) {
		clearText();
		document.form.textwiew.value = operation + ' ';

		if (isEqual && !operationActive) {
			firstOperand = 0;
			operation = '';
		}
		operationActive = false;
		isEqual = false;
		isDot = false;
	}

	result = document.form.textwiew.value;
	result = String(result);
	if (result.length < 14)
	{
		if (result == '0') 
			{ back(); }
		document.form.textwiew.value = document.form.textwiew.value + num;
		isEqual = false;
	}
}

function equal() {
	var value = document.form.textwiew.value;
	if (!firstOperand)
		return;
	if (value && !isEqual ){
		result = eval(firstOperand + document.form.textwiew.value);
		if (result % 1 > 0) {
			result = String(result);
			result = result.substring(0,14);
		}
		document.form.textwiew.value = result;
		operation = '';
		firstOperand = document.form.textwiew.value;
		isEqual = true;
	}
}

function clearText() {
	document.form.textwiew.value = '';
	isDot = false;
}

function clearAll() {
	clearText();
	firstOperand = 0;
	operation = '';
	operationActive = false;
}

function back() {
	var value = document.form.textwiew.value,
		i = value.length - 1,
		doubleBack = false;
	if (value[i] == '.')
		isDot = false;
	if ((value[i] == '+') || (value[i] == '-') || (value[i] == '*') || (value[i] == '/')) {
		operationActive = false;
		firstOperand = 0;
		operation = '';
		doubleBack = true;
	}
	document.form.textwiew.value = value.substring(0,value.length - 1);
	if (doubleBack)
		back();

}

function sqrt() {
	var value = document.form.textwiew.value;
	if (value && firstOperand && operation)
		{
			equal();
		}
	if (value) {
		result = Math.sqrt(document.form.textwiew.value);
		result = String(result);
		result = result.substring(0,14);
		document.form.textwiew.value = result;
	}
}

function sqr() {
	var value = document.form.textwiew.value;
	if (value && firstOperand && operation)
		{
			equal();
		}
	if (value)
	document.form.textwiew.value = document.form.textwiew.value * document.form.textwiew.value;
}


document.addEventListener('keydown', function(e) {
	if (e.keyCode == 32)
		return;
	if (e.key == 1 || e.key == 2 || e.key == 3 || e.key == 4 || e.key == 5 || e.key == 6 || e.key == 7 || e.key == 8 || e.key == 9 || e.key == 0 || e.key == '-' || e.key == '+' || e.key == '*' || e.key == '/' || e.key == '.')
		insert(e.key);
	if (e.key == ',')
		insert('.')
	if (e.keyCode == 13 || e.key == '=') 
		equal();
	if (e.keyCode == 8)
		back();
	if (e.keyCode == 27 || e.keyCode == 46)
		clearAll();
});
